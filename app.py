from flask import Flask
from flask import request
from flask_cors import CORS

from core.sqlalchemy import db
from config.db import URI
from resources.user import User

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
CORS(app, supports_credentials=True)

@app.route('/user/', methods=['GET', 'POST'])
def user():
    user = User()

    if request.method == 'GET':
        return user.get_all()
    
    if request.method == 'POST':
        return user.post()

@app.route('/user/<_id_>', methods=['GET', 'PATCH', 'DELETE'])
def user_id(_id_):
    user = User()

    if request.method == 'GET':
        return user.get(_id_)
    
    if request.method == 'PATCH':
        return user.patch(_id_)
    
    if request.method == 'DELETE':
        return user.delete(_id_)

if __name__ == '__main__':
	app.run(use_debugger=True, host= "0.0.0.0", port=8888, use_reloader=True)
