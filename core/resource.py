from json import dumps
from flask.wrappers import Response

class BaseResource(object):
	def make_response(self, response, code=200, header=None, mimetype='application/json'):
		if response.__class__ == Response().__class__: #if response is a flask.wrappers.Response class
			response.status_code = code
			response.mimetype = mimetype

			if header:
				response.headers.extend(header)
				

		else: #if response is a dict
			response = dumps(response)
			response = Response(response, status=code, mimetype=mimetype)

			if header:
				for key, value in header.items():
					response.headers[key] = value

		return response