from sqlalchemy import Column
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy.dialects.mysql import TIMESTAMP

from core.sqlalchemy import db

class User(db.Model):
    _id_ = Column('id', BIGINT(unsigned=True), primary_key=True)
    username = Column(VARCHAR(64))
    password = Column(VARCHAR(1024))
    email = Column(VARCHAR(128))
    creation_date = Column(TIMESTAMP())