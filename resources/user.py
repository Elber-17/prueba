from json import dumps

from flask import request

from core.resource import BaseResource
from core.sqlalchemy import db
from models.user import User as ModelUser


class User(BaseResource):
    def get_all(self):
        users = ModelUser.query.all()
        response_array = []

        for user in users:
            response_object = {
                'id': user._id_,
                'username': user.username,
                'password': user.password,
                'email': user.email,
                'creationDate': str(user.creation_date.date())
            }

            response_array.append(response_object)

        return self.make_response(response_array, 200)

    def get(self, _id_):
        user = ModelUser.query.get(_id_)

        if not user:
            return self.make_response({'Not Found' : 'Este usuario no existe'}, 404)    

        response = {
            'id': user._id_,
            'username': user.username,
            'password': user.password,
            'email': user.email,
            'creationDate': str(user.creation_date.date())
        }

        return self.make_response(response, 200)
    
    def post(self):
        if not request.args.get('username', False):
            return self.make_response({'Bad Request' : "Falta el parametro 'username'"}, 400)
        
        if not request.args.get('password', False):
            return self.make_response({'Bad Request' : "Falta el parametro 'password'"}, 400)
        
        if not request.args.get('email', False):
            return self.make_response({'Bad Request' : "Falta el parametro 'email'"}, 400)
        
        username = request.args.get('username')
        password = request.args.get('password')
        email = request.args.get('email')
        user = ModelUser(username=username, password=password, email=email)

        db.session.add(user)
        db.session.commit()
        response = {"Ok" : "Usuario Registrado"}

        return self.make_response(response, 201)

    
    def patch(self, _id_):
        user_id = _id_
        
        username = request.args.get('username', False);
        password = request.args.get('password', False);
        email = request.args.get('email', False);

        if not username and not password and not email:
            return self.make_response({'Bad Request' : 'No has enviado parametros para actualizar'}, 400)

        user = ModelUser.query.get(user_id)

        if not user:
            return self.make_response({'Not Found' : 'Este usuario no existe'}, 404)
        
        if username:
            user.username = username

        if email:
            user.email = email
        
        if password:
            user.password = password

        db.session.add(user)
        db.session.commit()

        return self.make_response({}, 204)

    
    def delete(self, _id_):
        user_id = _id_
        user = ModelUser.query.get(user_id)

        if not user:
            return self.make_response({'Not Found' : 'Este usuario no existe'}, 404)

        db.session.delete(user)
        db.session.commit()

        return self.make_response({}, 204)